# OTManifoldLearning
## Badges
### Private CRC Binderhub Links
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://c111-004.cloud.gwdg.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fa04_ot_manifold_learning/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://c111-004.cloud.gwdg.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fa04_ot_manifold_learning/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://c111-004.cloud.gwdg.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fa04_ot_manifold_learning/HEAD?urlpath=voila)

### Public CRC Binderhub Links
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fa04_ot_manifold_learning/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fa04_ot_manifold_learning/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fa04_ot_manifold_learning/HEAD?urlpath=voila)

### JupyterLite Link
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://crc1456.pages.gwdg.de/livedocs/a04_ot_manifold_learning)

### Static HTMLs
[![static-html](https://img.shields.io/badge/CRC1456-QuadOTExplanation-white)](https://crc1456.pages.gwdg.de/livedocs/a04_ot_manifold_learning/files/QuadOTExplanation.html)

### Docker Image at GWDG Gitlab Docker Registry
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/crc1456/livedocs/a04_ot_manifold_learning/container_registry/)

-----